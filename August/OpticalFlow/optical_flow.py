import cv2
import numpy as np

font = cv2.FONT_HERSHEY_SIMPLEX


def calc_step(x, y, img):
    """Calculate horizontal and vertical steps."""
    x_step = img.shape[0] // x
    y_step = img.shape[1] // y
    return x_step, y_step


def get_points(x_step, y_step, img):
    """Calculate the coordinates of the points to track."""
    points = []
    for i in np.arange(0, img.shape[0] - x_step // 2, x_step):
        for j in np.arange(0, img.shape[1] - y_step // 2, y_step):
                points.append([y_step / 2 + j, x_step / 2 + i])
    points = np.float32(points)
    return points


def track_lkt(x_step, y_step, lk_params, points):
    """Apply Lucas-Kanade tracker to given points."""
    n = len(points)
    global old_gray
    old_gray = cv2.resize(old_gray, (640, 480))
    while 1:

        # read the frame and convert to grayscale
        ret, frame = cap.read()
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # draw lines
        for i in np.arange(x_step, 480, x_step):
            cv2.line(frame, (0, i), (640, i), (255, 0, 0), 1)
        for j in np.arange(y_step, 640, y_step):
            cv2.line(frame, (j, 0), (j, 480), (255, 0, 0), 1)

        # calculate the flow
        flow = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, points, None,
                                        **lk_params)[0]
        # calculate the magnitude of the flow vectors and choose
        # the ones with magnitude >= 3 (to show only meaningfull vectors)
        mag = [cv2.norm(flow[i] - points[i]) for i in range(n)]
        flow = [flow[i] for i in range(n) if mag[i] >= 5]
        if len(flow) > 0:

            # choose corresponding points of interest
            val_points = [points[i] for i in range(n) if mag[i] >= 5]

            # calculate scaling factor
            mag = [i for i in mag if i >= 5]
            maxnorm = max(mag)
            scale = maxnorm / 30

            # calculate scaled flow
            flow = [[val_points[i][0] + (flow[i][0] - val_points[i][0]) / scale,
                     val_points[i][1] + (flow[i][1] - val_points[i][1]) / scale] for i in range(len(mag))]

            # Calculate the color palette for the flow vectors and draw them.
            # The colors are chosen from the hsv colorspace, where
            # saturation and value are set to 255 and hue is changing depending
            # on the magnitude of the flow vector. Minimum hue is set to 120
            # which corresponds to blue color and maximum is set to 180 which
            # corresponds to red color.
            norm_mag = cv2.normalize(np.array(mag), None, 120, 180,
                                     cv2.NORM_MINMAX)
            hsv = [np.uint8([norm_mag[i], 255, 255]) for i in range(len(mag))]
            for i in range(len(mag)):
                hsv[i].shape = (1, 1, 3)
                color = cv2.cvtColor(hsv[i], cv2.COLOR_HSV2BGR)
                color.shape = (3, )
                color = [int(i) for i in color]
                cv2.arrowedLine(frame, tuple(np.uint16(val_points[i])),
                                tuple(np.uint16(flow[i])),
                                color, 2, tipLength=0.5)
        # save current frame for next iteration
        old_gray = frame_gray.copy()

        cv2.putText(frame, "Lucas-Kanade", (15, 40), font, 0.5,
                    (255, 255, 255), 1, cv2.LINE_AA)
        cv2.imshow("frame", frame)
#        out.write(frame)

        # initialize key to switch from LKT to Farneback or to close window
        global key
        key = cv2.waitKey(20)
        if key == ord("q"):
            cv2.destroyAllWindows()
            break
        elif key == ord("f"):
            break


def track_farn(x_step, y_step, fn_params, points):
    """Apply Farneback tracke to given points."""
    n = len(points)
    global old_gray
    old_gray = cv2.resize(old_gray, (400, 300))
    while 1:
        ret, frame = cap.read()
        frame = cv2.resize(frame, (400, 300))
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        for i in np.arange(x_step, 300, x_step):
            cv2.line(frame, (0, i), (400, i), (255, 0, 0), 1)
        for j in np.arange(y_step, 400, y_step):
            cv2.line(frame, (j, 0), (j, 300), (255, 0, 0), 1)
        flow = cv2.calcOpticalFlowFarneback(old_gray, frame_gray, None,
                                            **fn_params)
        flow = [flow[int(points[i][1])][int(points[i][0])] for i in range(n)]

        mag = [cv2.norm(flow[i]) for i in range(n)]

        flow = [flow[i] for i in range(n) if mag[i] >= 3]
        if len(flow) > 0:
            val_points = [points[i] for i in range(n) if mag[i] >= 3]
            mag = [i for i in mag if i >= 3]
            maxnorm = max(mag)

            scale = 20 / maxnorm

            flow = [np.uint16(val_points[i] + flow[i]) for i in range(len(mag))]

            flow = [[val_points[i][0] + (flow[i][0] - val_points[i][0]) * scale,
                     val_points[i][1] + (flow[i][1] - val_points[i][1]) * scale] for i in range(len(mag))]

            norm_mag = cv2.normalize(np.array(mag), None, 120, 180,
                                     cv2.NORM_MINMAX)
            hsv = [np.uint8([norm_mag[i], 255, 255]) for i in range(len(mag))]
            for i in range(len(mag)):
                hsv[i].shape = (1, 1, 3)
                color = cv2.cvtColor(hsv[i], cv2.COLOR_HSV2BGR)
                color.shape = (3, )
                color = [int(i) for i in color]
                cv2.arrowedLine(frame, tuple(np.uint16(val_points[i])),
                                tuple(np.uint16(flow[i])),
                                color, 2, tipLength=0.5)
        old_gray = frame_gray.copy()
        frame = cv2.resize(frame, (640, 480))
        cv2.putText(frame, "Farneback", (15, 40), font, 0.5,
                    (255, 255, 255), 1, cv2.LINE_AA)
        cv2.imshow("frame", frame)
#        out.write(frame)
        global key
        key = cv2.waitKey(20)
        if key == ord('q'):
            cv2.destroyAllWindows()
            break
        elif key == ord("l"):
            break


def show_both(nx, ny, lk_params, fn_params):
    """Display the optical flow for given points using Lucas-Kanade and
    Farneback methods.

    Arguments:
    nx, ny -- number of blocks along vertical and horizontal axis
    lk_params -- dictionary of parameters for Lucas-Kanade method
    fn_params -- dictionary of parameters for Farneback method
    """

    global old_gray, cap, key, out
    # read first frame and convert to grayscale
    cap = cv2.VideoCapture(0)
#    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
#    out = cv2.VideoWriter('August\\OpticalFlow\\OpticalFlowTest.avi',
#                          fourcc, 20.0, (640, 480))
    old_frame = cap.read()[1]
    old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)

    # calculate steps
    x_steplk, y_steplk = calc_step(nx, ny, old_gray)
    x_stepfarn, y_stepfarn = calc_step(nx, ny, cv2.resize(old_gray,
                                                          (400, 300)))

    # initialize points of interest
    points_lk = get_points(x_steplk, y_steplk, old_gray)
    points_farn = get_points(x_stepfarn, y_stepfarn, cv2.resize(old_gray,
                                                                (400, 300)))

    # Create window to display flow and intialize key to switch between
    # flow methods, "f" corresponds to Farneback, "l" - to Lucas-Kanade and
    # "q" to stop the process.
    # Start with Lucas-Kanade.
    cv2.namedWindow("frame", cv2.WINDOW_NORMAL)
    key = ord("l")
    while 1:
        if key == ord("f"):
            track_farn(x_stepfarn, y_stepfarn, fn_params, points_farn)
            continue
        elif key == ord("l"):
            track_lkt(x_steplk, y_steplk, lk_params, points_lk)
            continue
        elif key == ord("q"):
            break
#    out.release()


nx, ny = 7, 7

# Lucas-Kanade parameters
lk_params = {"winSize": (30, 30),
             "maxLevel": 3,
             "criteria": (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,
                          10, 0.04)}

# Farneback parameters
fn_params = {"pyr_scale": 0.5,
             "levels": 2,
             "winsize": 4,
             "iterations": 1,
             "poly_n": 5,
             "poly_sigma": 1.1,
             "flags": 0}


show_both(nx, ny, lk_params, fn_params)
