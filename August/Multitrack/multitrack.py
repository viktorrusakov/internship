import cv2
import numpy as np
import time
from random import randint

font = cv2.FONT_HERSHEY_SIMPLEX
trackerTypes = ["CSRT", "MEDIANFLOW", "MOSSE"]


class trackableObject:
    def __init__(self, ID, centroid, rect, color):
        # store the object ID, centroids, rectangle coordinates and color
        self.ID = ID
        self.centroid = [np.array(centroid)]
        self.rect = rect
        self.color = color


def preprocess(frame, prev_frame):
    """Perform preprocessing steps before detection."""

    # Calculate abolute difference beetween current and previous frames
    abs_diff = cv2.absdiff(frame, prev_frame)

    # Convert to grayscale
    gray = cv2.cvtColor(abs_diff, cv2.COLOR_BGR2GRAY)

    # Apply image thresholding
    th = cv2.threshold(gray, 15, 255, cv2.THRESH_BINARY)[1]

    # Apply dialation
    thresh = cv2.dilate(th, None, iterations=1)
    return thresh


def createTrackerByName(trackerType):
    # Create a tracker based on tracker name
    if trackerType == trackerTypes[0]:
        tracker = cv2.TrackerCSRT_create()
    elif trackerType == trackerTypes[1]:
        tracker = cv2.TrackerMedianFlow_create()
    elif trackerType == trackerTypes[2]:
        tracker = cv2.TrackerMOSSE_create()
    else:
        cv2.destroyAllWindows()
        raise ValueError("Could not find {} in trackerTypes. ".format(
                trackerType) + "Available trackers are {}, {}, {}.".format(
                                 "CSRT", "MEDIANFLOW", "MOSSE"))
    return tracker


def trackObjects(trackerType="CSRT", videoPath="hockey.mp4", write=False,
                 videoName="test.avi", players=4, showTrajectory=False):
    """Track players in the hockey game in video feed.

    Arguments:
    trackerType -- type of the tracker to use, available are: "CSRT", "MOSSE",
    "MEDIANFLOW"
    videoPath -- path to the video
    write -- set to True to record the video
    videoName -- name of the file under which the video should be recorded
    players -- number of players to track
    showTrajectory -- if set to True shows trajectories of tracked objects.
    """

    # If number of players to track is set to a value bigger than max number
    # of player on the field, track all the players
    if players > 13:
        players = 13

    # Create MultiTracker object
    multiTracker = cv2.MultiTracker_create()

    # Read the first frame and resize it
    cap = cv2.VideoCapture(videoPath)
    res, prev_frame = cap.read()
    prev_frame = cv2.resize(prev_frame, (640, 480))

    # Initialize video recording if write is set to True
    if write:
        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        out = cv2.VideoWriter('{}'.format(videoName), fourcc, 20.0, (640, 480))

    trackableObjects = []
    frames = 0
    # Process video and track objects
    while 1:

        # Start reading frames
        start = time.time()
        success, frame = cap.read()

        # Terminate process if failed to read frames
        if not success:
            cv2.destroyAllWindows()
            print("Failed to read video.")
            break

        frames += 1

        # Resize frame
        frame = cv2.resize(frame, (640, 480))

        # Update tracker and calculate new rectangles and centroids of players
        ret, rects = multiTracker.update(frame)
        rects = list(rects)
        for i, rect in enumerate(rects):
            (x, y, w, h) = [int(v) for v in rect]
            trackableObjects[i].rect = (x, y, w, h)
            trackableObjects[i].centroid.append(np.array([int(x + w/2),
                                                          int(y + h/2)]))

        # If not already tracking required amount of players, detect new ones
        if len(rects) < players:

            # Perform preprocessing
            img = preprocess(frame, prev_frame)

            # If not tracking any players
            if not rects:

                # Find contours and define big ones as players
                contours = cv2.findContours(img, cv2.RETR_EXTERNAL,
                                            cv2.CHAIN_APPROX_SIMPLE)[1]
                persons = [ctn for ctn in contours if
                           cv2.contourArea(ctn) > 200]

                # If found more players than required, choose only the required
                # amount
                if len(persons) >= players:
                    persons = persons[:players]

                # Add players to tracker
                for i in range(len(persons)):
                    x, y, w, h = cv2.boundingRect(persons[i])
                    color = (randint(0, 255), randint(0, 255), randint(0, 255))
                    trackableObjects.append(trackableObject(
                            i + 1, [int(x + w/2), int(y + h/2)], (x, y, w, h),
                            color))
                    multiTracker.add(createTrackerByName(trackerType), frame,
                                     (x, y, w, h))
            else:
                # Find big contours and calculate their centroids
                contours = cv2.findContours(img, cv2.RETR_EXTERNAL,
                                            cv2.CHAIN_APPROX_SIMPLE)[1]
                contours = [cnt for cnt in contours if cv2.contourArea(cnt) > 200]
                moments = [cv2.moments(cnt) for cnt in contours]
                cnt_centroids = [(int(M['m10']/M['m00']),
                                  int(M['m01']/M['m00'])) for M in moments]

                # For each centroid calculate its min distance to tracked
                # centroids. If centroid is far enough from tracked players,
                # consider it a new player.
                for centr in cnt_centroids:
                    mindist = min([cv2.norm(
                            np.array(centr), obj.centroid[-1]) for obj
                            in trackableObjects])
                    if mindist >= 30:
                        ind = cnt_centroids.index(centr)
                        x, y, w, h = cv2.boundingRect(contours[ind])
                        color = (randint(0, 255), randint(0, 255),
                                 randint(0, 255))
                        trackableObjects.append(trackableObject(
                                len(rects) + 1, [int(x + w/2), int(y + h/2)],
                                (x, y, w, h), color))
                        multiTracker.add(createTrackerByName(trackerType),
                                         frame, (x, y, w, h))

                        # Stop if reached required amount of players to track
                        if len(trackableObjects) == players:
                            break
            status = "Detecting"

        # If found all players
        else:
            status = "Tracking"

            # Perform detection every 5 frames to check for failures in tracking
            if frames >= 5:
                status = "Detecting"
                img = preprocess(frame, prev_frame)
                contours = cv2.findContours(img, cv2.RETR_EXTERNAL,
                                            cv2.CHAIN_APPROX_SIMPLE)[1]

                # Find contours and their centroids that correspond to motion
                motion = [ctn for ctn in contours if cv2.contourArea(ctn) > 20]
                motion_moments = [cv2.moments(cnt) for cnt in motion]
                motion_cent = [(int(M['m10']/M['m00']),
                                int(M['m01']/M['m00'])) for M in motion_moments]

                # Perform the same steps for big contours (potentially lost
                # players)
                contours = [ctn for ctn in contours if cv2.contourArea(ctn) > 150]
                moments = [cv2.moments(cnt) for cnt in contours]
                cnt_centers = [(int(M['m10']/M['m00']),
                                int(M['m01']/M['m00'])) for M in moments]

                # Check if tracker has failed
                for i in range(len(trackableObjects)):

                    # For currently tracked player calculate the closest
                    # motion to his centroid
                    dist = [cv2.norm(trackableObjects[i].centroid[-1],
                                     np.array(cnt)) for cnt in motion_cent]
                    if not dist:
                        continue
                    min_dist = min(dist)

                    # If there is no motion close to centroid -- the tracker
                    # has lost the player
                    if min_dist > 15:

                        # Calculate the distances from lost player's centroid
                        # to all found person contours and sort in ascending
                        # order
                        dist = [cv2.norm(trackableObjects[i].centroid[-1],
                                         np.array(cnt)) for cnt in cnt_centers]
                        dist_sorted = dist.copy()
                        dist_sorted.sort()

                        # Starting from the closest contour check if it is
                        # already being tracked. If not -- assing this contour
                        # to lost player
                        for j in range(len(dist)):
                            ind = dist.index(dist_sorted[j])
                            player_dist = [cv2.norm(np.array(cnt_centers[ind]),
                                                    obj.centroid[-1])
                                           for obj in trackableObjects
                                           if obj != trackableObjects[i]]
                            min_dist = min(player_dist)
                            if min_dist > 25:
                                trackableObjects[i].centroid[-1] = np.array(
                                        cnt_centers[ind])
                                trackableObjects[i].rect = cv2.boundingRect(
                                        contours[ind])
                                break

                # Re-initialize tracker
                multiTracker = cv2.MultiTracker_create()
                for obj in trackableObjects:
                    multiTracker.add(createTrackerByName(trackerType), frame,
                                     tuple(obj.rect))

                # Reset frame count
                frames = 0

        # Update previous frame
        prev_frame = frame.copy()

        # Draw tracked players
        for obj in trackableObjects:
            (x, y, w, h) = obj.rect
            cv2.rectangle(frame, (x, y), (x + w, y + h), obj.color, 2)
            cv2.putText(frame, "{}".format(obj.ID), (x, y - 5), font, 0.5,
                        obj.color, 2, cv2.LINE_AA)

        # Draw trajectories
        if showTrajectory:
            for obj in trackableObjects:
                for i in range(len(obj.centroid) - 1):
                    cv2.line(frame, tuple(obj.centroid[i]),
                             tuple(obj.centroid[i + 1]), obj.color, 3)

        # Calculate fps and add some text to video
        fps = round(1 / (time.time() - start)) if start != time.time() else 0
        cv2.putText(frame, "Tracker: {},".format(trackerType) + " " +
                    "Players: {}/{},".format(len(trackableObjects), players) +
                    " " + "FPS: {}, ".format(fps) + "Trajectory: {} ".format(
                    "ON" if showTrajectory else "OFF"), (10, 20), font, 0.5,
                    (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame, "Status: {}".format(status), (10, 40), font, 0.5,
                    (255, 255, 255), 1, cv2.LINE_AA)

        # Show frame
        cv2.imshow("Frame", frame)

        # If recording, record frame to video
        if write:
            out.write(frame)

        # Create key to control video depending on what button is pressed.
        # "1" -- change to CSRT tracker, "2" -- change to MEDIANFLOW tracker,
        # "3" -- change to MOSSE tracker, "t" -- show trajectories,
        # "q" -- close video player
        key = cv2.waitKey(1) & 0xFF
        if key == ord("1"):
            trackerType = "CSRT"
            multiTracker = cv2.MultiTracker_create()
            for obj in trackableObjects:
                multiTracker.add(createTrackerByName(trackerType), frame,
                                 tuple(obj.rect))
        elif key == ord("2"):
            trackerType = "MEDIANFLOW"
            multiTracker = cv2.MultiTracker_create()
            for obj in trackableObjects:
                multiTracker.add(createTrackerByName(trackerType), frame,
                                 tuple(obj.rect))
        elif key == ord("3"):
            trackerType = "MOSSE"
            multiTracker = cv2.MultiTracker_create()
            for obj in trackableObjects:
                multiTracker.add(createTrackerByName(trackerType), frame,
                                 tuple(obj.rect))
        elif key == ord("t"):
            showTrajectory = not showTrajectory
        elif key == ord("q"):
            cv2.destroyAllWindows()
            break

    # If recording -- finish
    if write:
        out.release()


trackObjects(trackerType="MEDIANFLOW", players=10)
