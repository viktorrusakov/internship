import cv2
import numpy as np


def show_image(img):
    if type(img) is list:
        for i in range(len(img)):
            cv2.imshow("image{}".format(i + 1), img[i])
    else:
        cv2.imshow("image", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def kmeans_img(img, K=2, eps=1, maxIter=10, tries=10):
    """Apply k-means to an image."""
    img_reshaped = img.reshape((-1, 3))
    img_reshaped = np.float32(img_reshaped)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
                maxIter, eps)
    ret, label, center = cv2.kmeans(img_reshaped, K, None, criteria, tries,
                                    cv2.KMEANS_RANDOM_CENTERS)
    center = np.uint8([cv2.randu(i, 0, 255) for i in center])
    res = center[label.flatten()]
    res = res.reshape((img.shape))
    return res


test = cv2.imread("source.png")
kmeans_test = kmeans_img(test, K=3)
show_image(kmeans_test)


def binary_kmeans(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    colors = np.unique(gray)
    images = []
    for i in range(len(colors)):
        temp = ([255 if j == colors[i] else 0 for j in gray.flatten()])
        temp = np.uint8(temp)
        temp = temp.reshape((gray.shape))
        images.append(temp)
    return images


binary_test = binary_kmeans(kmeans_test)
show_image(binary_test)
show_image(kmeans_test)


def draw_contour(binaryImg, sourceImg):
    contours_binary = []
    contours_source = []
    for img in binaryImg:
        contours = cv2.findContours(img, cv2.RETR_TREE,
                                    cv2.CHAIN_APPROX_SIMPLE)[1]
        contours_binary.append(cv2.drawContours(img.copy(), contours,
                                                -1, 125, 2))
        contours_source.append(cv2.drawContours(sourceImg.copy(), contours,
                                                -1, (255, 255, 255), 2))
    return contours_binary, contours_source


binary, source = draw_contour(binary_test, kmeans_test)

show_image(kmeans_test)
show_image(source)
show_image(binary)

for i in range(len(binary)):
    cv2.imwrite("August\\contours\\binary_contour{}.png".format(i + 1),
                binary[i])
    cv2.imwrite("August\\contours\\source_contour{}.png".format(i + 1),
                source[i])

# Palettes

show_image(test)

# Cool palette
cool_palette = cv2.applyColorMap(test, cv2.COLORMAP_COOL)
cv2.imwrite("August\\contours\\Palettes\\cool.png", cool_palette)
show_image(cool_palette)

# Rainbow
rainbow_palette = cv2.applyColorMap(test, cv2.COLORMAP_RAINBOW)
cv2.imwrite("August\\contours\\Palettes\\rainbow.png", rainbow_palette)
show_image(rainbow_palette)

# Ocean
ocean_palette = cv2.applyColorMap(test, cv2.COLORMAP_OCEAN)
cv2.imwrite("August\\contours\\Palettes\\ocean.png", ocean_palette)
show_image(ocean_palette)
