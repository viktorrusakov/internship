import cv2
import numpy as np
import time
import imageio

# Load calibration data
calib = np.load("test.npz")
calib_mtx = calib["mtx"]
calib_dist = calib["dist"]


font = cv2.FONT_HERSHEY_SIMPLEX


def draw(img, imgpts):
    "Draw cube with given vertices."

    imgpts = np.int32(imgpts).reshape(-1, 2)
    img = cv2.drawContours(img, [imgpts[:4]], -1, (0, 255, 0), 3)
    for i, j in zip(range(4), range(4, 8)):
        img = cv2.line(img, tuple(imgpts[i]), tuple(imgpts[j]), (255, 0, 0), 3)
    img = cv2.drawContours(img, [imgpts[4:]], -1, (0, 0, 255), 3)
    return img


class Marker():
    def __init__(self, ID, keypoints, descriptors, algName,  matches=None,
                 dist_sum=None, dist_mean=None):
        self.ID = ID
        self.keypoints = keypoints
        self.descriptors = descriptors
        self.matches = matches
        self.dist_sum = dist_sum
        self.dist_mean = dist_mean
        self.algName = algName

    def good_matches(self):
        good = []
        if self.matches is None:
            return None
        for match in self.matches:
            if len(match) == 1 or not match:
                continue
            m, n = match
            if self.algName == "sift":
                if m.distance < 200 and m.distance < 0.7*n.distance:
                    good.append(m)
            elif self.algName == "surf":
                if m.distance < 5 and m.distance < 0.7*n.distance:
                    good.append(m)
            else:
                if m.distance < 65 and m.distance < 0.7*n.distance:
                    good.append(m)
        if len(good) < 20:
            good = None
        else:
            good = sorted(good, key=lambda x: x.distance)
            good = good[:50]
        self.matches = good

    def calc_dist(self):
        if self.matches is None:
            self.dist_sum = None
            self.dist_mean = None
            return None
        dist = 0
        for match in self.matches:
            dist += match.distance
        self.dist_sum = dist
        self.dist_mean = round(dist / len(self.matches), 2)


def initializeAlg(algName):
    """Initialize algorithm by name."""

    if algName == "sift":
        alg = cv2.xfeatures2d_SIFT.create(nOctaveLayers=4, sigma=1.5)
    elif algName == "surf":
        alg = cv2.xfeatures2d_SURF.create()
    elif algName == "orb":
        alg = cv2.ORB_create(nlevels=12)
    return alg


def calcDescr(algName, image):
    """Detect keypoints  and calculate descriptors for them.

    Arguments:
    algName -- name of the algorithm to use. The argument should be passed as
    a string or a list of 2 strings (in which case an algorithm with first name
    is used to detect keypoints and algorithm with second name -- to calculate
    descriptors)
    frame -- a frame to find keypoints on
    """

    alg = initializeAlg(algName)
    keypoints, descr = alg.detectAndCompute(image, None)
    return keypoints, descr


def get_params(algName):
    """Return the parameters for match search based on the algorithm's name."""

    if algName == "orb":
        FLANN_INDEX_LSH = 6
        index_params = dict(algorithm=FLANN_INDEX_LSH,
                            table_number=6,
                            key_size=12,
                            multi_probe_level=1)
    else:
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    return index_params


def multiDetection(algName="sift", write=False, videoName="MultipleDetect.mp4",
                   showScale=False):
    """Shows homography and Id of an object from web cam.

    Arguments:
    algName --name of the algorithm to use. The argument should be passed as
    a string or a list of 2 strings (in which case an algorithm with first name
    is used to detect keypoints and algorithm with second name -- to calculate
    descriptors)
    write -- set to True to record video
    videoName -- name of the video if recording
    showScale -- ste to True to show keypoints scales
    """

    if write:
        images = []
        writer = imageio.get_writer('{}'.format(videoName), fps=12)
    cap = cv2.VideoCapture(0)
    markers = []
    marker_images = []
    alg = initializeAlg(algName)
    for i in range(1, 6):
        image = cv2.imread("multipleMarkers\\{}.jpg".format(i), 0)
        keypoints, descr = calcDescr("{}".format(algName), image)
        markers.append(Marker(i, keypoints, descr, algName))
        marker_images.append(image)
    counter = 0  # for fps calcualtion
    fps = 0
    start = time.time()
    while 1:

        # Read frames
        ret, frame = cap.read()
        if not ret:
            print("Could not read frame.")
            break
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Initialize key to control video feed. Numbers 1 through 3 correspond
        # to changing algorithms being used, "c" -- show scale,
        # "q" -- quit video.
        key = cv2.waitKey(1)
        if key & 0xFF == ord("1"):
            algName = "sift"
            alg = initializeAlg(algName)
            for i in range(len(markers)):
                markers[i].keypoints, markers[i].descriptors = calcDescr(
                        "{}".format(algName), marker_images[i])
                markers[i].algName = algName
        elif key & 0xFF == ord("2"):
            algName = "surf"
            alg = initializeAlg(algName)
            for i in range(len(markers)):
                markers[i].keypoints, markers[i].descriptors = calcDescr(
                        "{}".format(algName), marker_images[i])
                markers[i].algName = algName
        elif key & 0xFF == ord("3"):
            algName = "orb"
            alg = initializeAlg(algName)
            for i in range(len(markers)):
                markers[i].keypoints, markers[i].descriptors = calcDescr(
                        "{}".format(algName), marker_images[i])
                markers[i].algName = algName
        elif key & 0xFF == ord("c"):
            showScale = not showScale
        elif key & 0xFF == ord('q'):
            break

        # Compute keypoints and descriptors for current frame
        kp, des = alg.detectAndCompute(gray, None)

        # Initialize parameters for matching
        index_params = get_params(algName)
        search_params = dict(checks=50)

        # Initialize matcher and find matches with it for each marker
        matcher = cv2.FlannBasedMatcher(index_params, search_params)
        for marker in markers:
            marker.matches = matcher.knnMatch(marker.descriptors, des, k=2)
            marker.good_matches()
            marker.calc_dist()

        # Choose possible markers
        possible_markers = []
        for marker in markers:
            if marker.dist_sum:
                possible_markers.append(marker)

        # Choose best match for found object in frame
        if possible_markers:
            best_match = sorted(possible_markers, key=lambda x: x.dist_sum,
                                reverse=True)[0]
            # Find homography
            new_kp1 = []
            new_kp2 = []
            for match in best_match.matches:
                new_kp1.append(best_match.keypoints[match.queryIdx])
                new_kp2.append(kp[match.trainIdx])
            src_pts = np.float32([best_match.keypoints[m.queryIdx].pt
                                  for m in best_match.matches]).reshape(-1, 1, 2)
            dst_pts = np.float32([kp[m.trainIdx].pt
                                  for m in best_match.matches]).reshape(-1, 1, 2)
            M, mask1 = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,
                                          5.0)
            # Solve for PnP to find projection of points to draw cube
            objpts1 = src_pts.copy()
            objpts1.shape = (-1, 2)
            objpts1 = np.array([np.append(pts, 1) for pts in src_pts],
                               dtype=np.float32)
            dstpts1 = dst_pts.copy()
            dstpts1 = np.array(dstpts1, dtype=np.float32)
            retval, rvecs, tvecs, inliers = cv2.solvePnPRansac(
                    objpts1, dstpts1, calib_mtx, calib_dist)
            lenght = marker_images[best_match.ID - 1].shape[0]
            axis = np.float32([[0, 0, 0], [0, lenght, 0], [lenght, lenght, 0],
                               [lenght, 0, 0], [0, 0, -lenght],
                               [0, lenght, -lenght], [lenght, lenght, -lenght],
                               [lenght, 0, -lenght]])
            imgpts, jac = cv2.projectPoints(axis, rvecs, tvecs, calib_mtx,
                                            calib_dist)
            frame = draw(frame, imgpts)
            # If found homography matrix, chech if hohomgraphy is good before
            # drawing it
            if isinstance(M, np.ndarray):
                x, y = 0, 0
                w = marker_images[best_match.ID - 1].shape[1]
                h = marker_images[best_match.ID - 1].shape[0]
                pts = np.float32([[x, y], [x, y + h], [x + w, y + h],
                                  [x + w, y]]).reshape(-1, 1, 2)
                dst = cv2.perspectiveTransform(pts, M)
                if cv2.isContourConvex(dst):
                    dst.shape = (4, 2)
                    ver_1 = np.linalg.norm(dst[0] - dst[1])
                    ver_2 = np.linalg.norm(dst[2] - dst[3])
                    hor_1 = np.linalg.norm(dst[0] - dst[3])
                    hor_2 = np.linalg.norm(dst[1] - dst[2])
                    y_ratio = ver_1 / ver_2
                    x_ratio = hor_1 / hor_2
                    if 0.7 < x_ratio < 1.3 and 0.7 < y_ratio < 1.3:
                        frame = cv2.polylines(frame, [np.int32(dst)], True,
                                              (255, 255, 255), 3, cv2.LINE_AA)
                cv2.putText(frame, "ID: {}".format(best_match.ID),
                            (frame.shape[1] - 150, 100), font, 0.5,
                            (255, 255, 255), 1, cv2.LINE_AA)
                # Show scaling if showScale is set to True
                if showScale:
                    for i in range(len(best_match.matches)):
                        best_match.matches[i].queryIdx = i
                        best_match.matches[i].trainIdx = i
                    draw_params = dict(color=-1,
                                       flags=4)
                    frame = cv2.drawKeypoints(frame, new_kp2, None,
                                              **draw_params)
                cv2.putText(frame, "Mean dist: {}".format(
                            best_match.dist_mean), (frame.shape[1] - 150, 80),
                            font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

        # Display some text depending on what algorithms are used
        cv2.putText(frame, "Algorithm: {}".format(algName.upper()),
                    (frame.shape[1] - 150, 40), font, 0.5,
                    (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(frame, "Show scale: {}".format(
                    "ON" if showScale else "OFF"),
                    (frame.shape[1] - 150, 60), font, 0.5,
                    (255, 255, 255), 1, cv2.LINE_AA)

        # Calculate fps and display it in frame
        counter += 1
        if (time.time() - start) >= 1:
            fps = round(counter / (time.time() - start))
            counter = 0
            start = time.time()
        cv2.putText(frame, "FPS: {}".format(fps), (frame.shape[1] - 150, 20),
                    font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

        # Show frame
        cv2.imshow('frame', frame)

        # Add frame to video if recording
        if write:
            images.append(frame[:, :, ::-1])
    cap.release()
    cv2.destroyAllWindows()

    # Save video if recording
    if write:
        for image in images:
            writer.append_data(image)
        writer.close()


multiDetection("orb", write=True, videoName="MultiDetection.mp4")
