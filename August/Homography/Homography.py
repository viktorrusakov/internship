import cv2
import numpy as np
import time
import imageio


font = cv2.FONT_HERSHEY_SIMPLEX


def initializeAlg(algName):
    """Initialize algorithm by name."""

    if algName == "sift":
        desc = cv2.xfeatures2d_SIFT.create()
    elif algName == "surf":
        desc = cv2.xfeatures2d_SURF.create()
    elif algName == "orb":
        desc = cv2.ORB_create()
    return desc


def calcDescr(algName, frame, mask):
    """Detect keypoints  and calculate descriptors for them.

    Arguments:
    algName -- name of the algorithm to use. The argument should be passed as
    a string or a list of 2 strings (in which case an algorithm with first name
    is used to detect keypoints and algorithm with second name -- to calculate
    descriptors)
    frame -- a frame to find keypoints on
    mask -- mask which specifies the segment of the frame where to search for
    keypoints
    """

    if len(algName) == 2:
        alg1 = initializeAlg(algName[0])
        alg2 = initializeAlg(algName[1])
        keypoints = alg1.detect(frame, mask)
        if algName[0] == "sift":
            keypoints = unpack_sift(keypoints)
        keypoints, descr = alg2.compute(frame, keypoints)
        return alg1, alg2, keypoints, descr
    alg = initializeAlg(algName)
    keypoints, descr = alg.detectAndCompute(frame, mask)
    return alg, keypoints, descr


def get_params(algName):
    """Return the parameters for match search based on the algorithm's name."""

    if len(algName) == 2:
        if algName[1] == "orb":
            FLANN_INDEX_LSH = 6
            index_params = dict(algorithm=FLANN_INDEX_LSH,
                                table_number=6,
                                key_size=12,
                                multi_probe_level=1)
        else:
            FLANN_INDEX_KDTREE = 0
            index_params = dict(algorithm=FLANN_INDEX_KDTREE,
                                trees=5)
    elif algName == "orb":
        FLANN_INDEX_LSH = 6
        index_params = dict(algorithm=FLANN_INDEX_LSH,
                            table_number=6,
                            key_size=12,
                            multi_probe_level=1)
    else:
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm=FLANN_INDEX_KDTREE,
                            trees=5)
    return index_params


def unpack_sift(keypoints):
    """Perform unpacking of the octave value from sift keypoints."""

    new = keypoints.copy()
    for k in new:
        k.octave = k.octave & 255
        if k.octave >= 128:
            if k.octave == 255:
                k.octave = 0
            else:
                k.octave |= -128
    return new


def showHomography(algName="sift", write=False, videoName="test2.mp4"):
    """Shows homography and matching for a cropped object from webcam.
    To crop an object press "s", crop desired object and press SPACE to confirm

    Arguments:
    algName --name of the algorithm to use. The argument should be passed as
    a string or a list of 2 strings (in which case an algorithm with first name
    is used to detect keypoints and algorithm with second name -- to calculate
    descriptors)
    write -- set to True to record video
    videoName -- name of the video if recording
    """

    # If recording, create writer object to write the video
    if write:
        images = []
        writer = imageio.get_writer('{}'.format(videoName), fps=10)
    cap = cv2.VideoCapture(0)
    keypoints = []
    counter = 0  # for fps calcualtion
    fps = 0
    start = time.time()
    while 1:

        # Read frames
        ret, frame = cap.read()
        if not ret:
            print("Could not read frame.")
            break
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Initialize key to control video feed. Numbers 1 through 9 correspond
        # to changing algorithms being used, "s" -- crop a region to find its
        # homography, "q" -- quit video.
        key = cv2.waitKey(1)
        if key & 0xFF == ord("s"):
            source_frame = frame.copy()

            # Crop a regin and find its vertices
            roi = cv2.selectROI("frame", frame)
            x, y, w, h = roi

            # Create a mask to specify the region of the frame where
            # keypoints need to be found
            mask = np.zeros(gray.shape, dtype=np.uint8)
            mask = cv2.rectangle(mask, (x, y), (x + w, y + h), 255, -1)

            # Initialize the algorithm and find keypoins with descriptors
            alg = initializeAlg(algName)
            keypoints, descr = alg.detectAndCompute(source_frame, mask)

            # Create an additional frame that show only the cropped region
            source = np.zeros((frame.shape), dtype=np.uint8)
            source[y:y + h, x:x + w] = frame[y:y + h, x:x + w]
            cv2.rectangle(source, (x, y), (x + w, y + h), (0, 255, 0), 1)
        elif key & 0xFF == ord("1"):
            algName = "sift"
            alg, keypoints, descr = calcDescr(algName, source_frame, mask)
        elif key & 0xFF == ord("2"):
            algName = "surf"
            alg, keypoints, descr = calcDescr(algName, source_frame, mask)
        elif key & 0xFF == ord("3"):
            algName = "orb"
            alg, keypoints, descr = calcDescr(algName, source_frame, mask)
        elif key & 0xFF == ord("4"):
            algName = ["orb", "sift"]
            alg1, alg2, keypoints, descr = calcDescr(algName, source_frame,
                                                     mask)
        elif key & 0xFF == ord("5"):
            algName = ["orb", "surf"]
            alg1, alg2, keypoints, descr = calcDescr(algName, source_frame,
                                                     mask)
        elif key & 0xFF == ord("6"):
            algName = ["sift", "surf"]
            alg1, alg2, keypoints, descr = calcDescr(algName, source_frame,
                                                     mask)
        elif key & 0xFF == ord("7"):
            algName = ["surf", "sift"]
            alg1, alg2, keypoints, descr = calcDescr(algName, source_frame,
                                                     mask)
        elif key & 0xFF == ord("8"):
            algName = ["surf", "orb"]
            alg1, alg2, keypoints, descr = calcDescr(algName, source_frame,
                                                     mask)
        elif key & 0xFF == ord("9"):
            algName = ["sift", "orb"]
            alg1, alg2, keypoints, descr = calcDescr(algName, source_frame,
                                                     mask)
        elif key & 0xFF == ord('q'):
            break

        # If found keypoints, perform update on current frame
        if keypoints:

            # In case of passing 2 algorithms preform detection with first one
            # and descriptors calculation with second one
            if len(algName) == 2:
                kp2 = alg1.detect(gray, None)

                # If detecting with sift, unpack octave value for keypoints
                if algName[0] == "sift":
                    kp2 = unpack_sift(kp2)
                kp2, des2 = alg2.compute(gray, kp2)
            else:
                kp2, des2 = alg.detectAndCompute(gray, None)

            # Initialize parameters for matching
            index_params = get_params(algName)
            search_params = dict(checks=50)

            # Initialize matcher and find matches with it
            matcher = cv2.FlannBasedMatcher(index_params, search_params)
            matches = matcher.knnMatch(descr, des2, k=2)

            # Leave only good matches
            good = []
            for match in matches:

                # If found ony one match or no matches, continue
                if len(match) == 1 or not match:
                    continue
                m, n = match

                # Criteria for good match
                if m.distance < 100 and m.distance < 0.75*n.distance:
                    good.append(m)

            # If found less than 15 good matches, don't show them
            if len(good) < 15:
                frame = np.hstack((source, frame))

            # Otherwise show the best 30 matches and object homography
            else:
                good = sorted(good, key=lambda x: x.distance)
                good = good[:30]
                src_pts = np.float32([keypoints[m.queryIdx].pt
                                      for m in good]).reshape(-1, 1, 2)
                dst_pts = np.float32([kp2[m.trainIdx].pt
                                      for m in good]).reshape(-1, 1, 2)
                M, mask1 = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,
                                              5.0)
                matchesMask = mask1.ravel().tolist()
                x, y, w, h = roi
                pts = np.float32([[x, y], [x, y + h], [x + w, y + h],
                                  [x + w, y]]).reshape(-1, 1, 2)
                dst = cv2.perspectiveTransform(pts, M)
                frame = cv2.polylines(frame, [np.int32(dst)], True,
                                      (255, 255, 255), 3, cv2.LINE_AA)
                draw_params = dict(matchColor=(0, 255, 0),
                                   singlePointColor=None,
                                   matchesMask=matchesMask,
                                   flags=2)
                frame = cv2.drawMatches(source, keypoints, frame, kp2, good,
                                        None, **draw_params)

            # Display some text depending on what algorithms are used
            if len(algName) == 2:
                cv2.putText(frame, "Detector: {}".format(algName[0].upper()),
                            (frame.shape[1] - 150, 40), font, 0.5,
                            (255, 255, 255), 1, cv2.LINE_AA)
                cv2.putText(frame, "Descriptor: {}".format(algName[1].upper()),
                            (frame.shape[1] - 150, 60), font, 0.5,
                            (255, 255, 255), 1, cv2.LINE_AA)
            else:
                cv2.putText(frame, "Algorithm: {}".format(algName.upper()),
                            (frame.shape[1] - 150, 40), font, 0.5,
                            (255, 255, 255), 1, cv2.LINE_AA)

        # If haven't found any keypoints yet, show the original frame
        else:
            source = np.zeros((frame.shape), dtype=np.uint8)
            frame = np.hstack((source, frame))
        counter += 1

        # Calculate fps and display it in frame
        if (time.time() - start) >= 1:
            fps = round(counter / (time.time() - start))
            counter = 0
            start = time.time()
        cv2.putText(frame, "FPS: {}".format(fps), (frame.shape[1] - 150, 20),
                    font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

        # Show frame
        cv2.imshow('frame', frame)

        # Add frame to video if recording
        if write:
            images.append(frame)
    cap.release()
    cv2.destroyAllWindows()

    # Save video if recording
    if write:
        for image in images:
            writer.append_data(image)
        writer.close()


showHomography("sift")
