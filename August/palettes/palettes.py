import cv2
import numpy as np


def show_image(img):
    if type(img) is list:
        for i in range(len(img)):
            cv2.imshow("image{}".format(i + 1), img[i])
    else:
        cv2.imshow("image", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


test = cv2.imread("source.png")

show_image(test)

# Cool palette
cool_palette = cv2.applyColorMap(test, cv2.COLORMAP_COOL)
cv2.imwrite("August\\palettes\\cool.png", cool_palette)
show_image(cool_palette)

# Rainbow
rainbow_palette = cv2.applyColorMap(test, cv2.COLORMAP_RAINBOW)
cv2.imwrite("August\\palettes\\rainbow.png", rainbow_palette)
show_image(rainbow_palette)

# Ocean
ocean_palette = cv2.applyColorMap(test, cv2.COLORMAP_OCEAN)
cv2.imwrite("August\\palettes\\ocean.png", ocean_palette)
show_image(ocean_palette)
