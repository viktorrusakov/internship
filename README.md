This is a repository for projects in the field of computer vision done during internship in IT-Jim company in July and August, 2018.
The projects are done using Python 3.6 and OpenCV package. All the work is divided into July and August folders, each containing
folders for projects done in respective month. Each project folder consists of .py file (source code) and test results such as
images (most of the tests were done on source.png) and videos.

The July folder contains following folders:  
kmeans -- implementation of kmeans algorithm to segment image by color;  
quantization -- quantize an image in certain amount of levels;  
haar_vs_lbp -- comparison of haar cascades classifier and lbp classifier.  

The August folder contains following folders:  
palletes -- apply pallete to an image;  
contours -- find and draw contours in an image after applying kmeans;  
OpticalFlow -- implementation of Lucas-Kanade and Farneback algorithms to show flow vectors during video stream;  
Multitrack -- track multiple objects in video stream;  
Homography -- show homography of an object in video stream.  
MultiDetection -- detect an object in video stream and show its unique ID. Also shows cube on top of a detected object.