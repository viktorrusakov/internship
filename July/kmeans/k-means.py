import cv2
import numpy as np
import imageio

font = cv2.FONT_HERSHEY_SIMPLEX


def kmeans_img(img, K=2, eps=1, maxIter=10, tries=10):
    """Apply k-means to an image.

    Arguments:
    img -- input image
    K -- amount of clusters
    eps -- accuracy for the kmeans algorithm
    maxIter -- maximum number of iterations
    tries -- number of times the kmeans algorithm used, returns the best result
    """

    img_reshaped = img.reshape((-1, 3)) if len(img.shape) == 3 else img.reshape((-1, 1))
    img_reshaped = np.float32(img_reshaped)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
                maxIter, eps)
    ret, label, center = cv2.kmeans(img_reshaped, K, None, criteria, tries,
                                    cv2.KMEANS_RANDOM_CENTERS)
    center = np.uint8([cv2.randu(i, 0, 255) for i in center])
    res = center[label.flatten()]
    res = res.reshape((img.shape))
    cv2.putText(res, "K={}".format(K), (10, 50), font, 0.8,
                (255, 255, 255), 1, cv2.LINE_AA)
    return res


def kmeans_gif(img, levels=5, K=2, dur=1,
               filename="July\\kmeans\\gif_kmeans.gif"):
    """Create a gif out of images with different amount of clusters.

    Arguments:
    img -- input image
    levels -- number of levels in gif
    dur -- duration of showing a single frame in gif
    filename -- the name of gif
    """

    images = []
    for i in range(levels):
        images.append(kmeans_img(img, K + i))
    imageio.mimsave("{}".format(filename), images, "GIF", duration=dur)


test = cv2.imread("source.png")
kmeans_test = kmeans_img(test, K=2)
cv2.imshow("kmeans", kmeans_test)
cv2.waitKey(0)
cv2.destroyAllWindows()
cv2.imwrite("July\\kmeans\\kmeans_test2.png", kmeans_test)

kmeans_gif(test, levels=5)
