import cv2
import numpy as np
import imageio

font = cv2.FONT_HERSHEY_SIMPLEX


def quant_img(img, N=5):
    """Quantize an image.

    Arguments:
    img -- grayscale image
    N -- amount of levels for quantization
    """

    scale = (img.max() - img.min() + 1) / N
    img = img // scale
    img = np.uint8(img * scale)
    cv2.putText(img, "K={}".format(N), (10, 50), font, 0.8,
                (255, 0, 0), 1, cv2.LINE_AA)
    return img


def quant_gif(img, levels=20, dur=1,
              filename="July\\quantization\\quantization.gif"):
    """Make a gif out of images with different levels of quantization.

    Arguments:
    img -- grayscale image
    levels -- number of levels in gif
    dur -- duration of showing a single frame in gif
    filename -- the name of gif
    """
    images = []
    for i in range(1, levels + 1):
        images.append(quant_img(img, 2 * i))
    imageio.mimsave("{}".format(filename), images, "GIF", duration=dur)


test = cv2.imread("source.png", 0)
quant_test = quant_img(test, N=10)
cv2.imshow("quant", quant_test)
cv2.waitKey(0)
cv2.destroyAllWindows()
cv2.imwrite("July\\quantization\\quant_test2.png", quant_test)

quant_gif(test, levels=10, filename="July\\quantization\\quant_gif1.gif")
