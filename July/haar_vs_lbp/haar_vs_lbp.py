import cv2
import numpy as np

lbp_cascade = cv2.CascadeClassifier(r'C:\Users\User\Anaconda3\pkgs\opencv-3.4.1-py36_200\Library\etc\lbpcascades\lbpcascade_frontalface_improved.xml')
haar_cascade = cv2.CascadeClassifier(r'C:\Users\User\Anaconda3\pkgs\opencv-3.4.1-py36_200\Library\etc\haarcascades\haarcascade_frontalface_default.xml')
font = cv2.FONT_HERSHEY_SIMPLEX


def take_pic(filename="pic.png"):
    cap = cv2.VideoCapture(0)
    frame = cap.read()[1]
    cv2.imwrite(filename, frame)


def show_image(img):
    cv2.imshow("image", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def lbp_detector(img, scaling=1.3, minNeighb=5):
    """Apply lbp face detector to an image."""
    img = img.copy()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = lbp_cascade.detectMultiScale(gray, scaling, minNeighb)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    return img


def haar_detector(img, scaling=1.3, minNeighb=5):
    """Apply haar face detector to an image."""
    img = img.copy()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = haar_cascade.detectMultiScale(gray, scaling, minNeighb)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    return img


img = cv2.imread("source.png")

test_lbp = lbp_detector(img)
show_image(test_lbp)
cv2.imwrite("July\\haar_vs_lbp\\test_lbp.png", test_lbp)

test_haar = haar_detector(img)
show_image(test_haar)
cv2.imwrite("July\\haar_vs_lbp\\test_haar.png", test_haar)
